
// ssh -i /Users/shaungillon/.ssh/hungrytail-server-key-pair.pem ubuntu@ec2-13-126-32-139.ap-south-1.compute.amazonaws.com

var express = require('express');
var cron = require('node-cron');
var fs = require('fs');
var tls = require('tls')
var cron = require('node-cron');
var MongoClient = require('mongodb').MongoClient;

const app = express();

//
// NOTE We use PM2 for running the server forever http://pm2.keymetrics.io/
// .... Use pm2 start ./bin/www on the server to run demon ....
//

//
// Store global websockets
//
var devices = []
var db;

//
// Connect to the shared mongo DB run and setup by METEOR
//
MongoClient.connect("mongodb://localhost:27017", function(err, client) {
  if(!err) {
    console.log("We are connected to mongo");
    db = client.db('hungrytail');
  } else {
    console.log("Error connecting to mongo " ,err);
  }
});

var options = {
  key: fs.readFileSync('/etc/letsencrypt/live/admin.hungrytail.com/privkey.pem', 'utf8'),
  cert: fs.readFileSync('/etc/letsencrypt/live/admin.hungrytail.com/fullchain.pem', 'utf8'),
};

const server = tls.createServer(options, function(socket){

  console.log('Server Connected as:', socket.authorized ? 'authorized' : 'unauthorized');

  socket.write('Meow!!\r\n');

  //
  // Set descriptive name
  //
  socket.name = socket.remoteAddress + ":" + socket.remotePort;

  //
  // Add device socket to array of available sockets
  //
  devices.push(socket);

  socket.setEncoding('utf8');

  socket.on('data', function(data) {
      console.log("DATA CAME FROM ", socket.name);

      //TODO on connection update device with the server information

      if(data == "" || data == undefined || data == null){
        return;
      }

      try {
      console.log(data);
       var message = JSON.parse(data.replace(/ 0+(?![\. }])/g, ' '));

       //
       // NOTE We use this to manage the server socket connection and ensure it is secure
       //
       if(!Array.isArray(message) && message.secret){
         socket['server_secret'] = message.secret == "97~-]HJRd.^W@)@;0qw8|W:0=1}g`R"
         return;
       }

       if(Array.isArray(message)){

         message.forEach(function(m) {

           //
           // HANDLE  FEED JSON feedback
           //
           if(m.feedTime && m.feedWeight){

             //TODO parse and write new
             console.log("We got a feed JSON");
           }

           //
           // Handle ACTIVITY JSON feedback
           //
           if(m.sTime && m.fTime && m.weight){

             console.log("We got a activity JSON", m);

             //TODO get ID from JSON
             if(parseInt(message.weight) >0){
                 db.collection('feeds').update(
                     { _id: 'sfdsfddsfsf' },
                     { $set: {
                       deviceId : socket.name,
                       sTime: new Date(), //var date = new Date(UNIX_Timestamp * 1000);
                       fTime: new Date(),
                       weight: parseInt(m.weight),
                     }}, { upsert: true }, function(err, result) {
                       console.log(err);
                 });
             }
           }

         });
       }

      } catch (e) {
        console.log("DATA PARSE FAILED....oh well", e);
      }

  });

  socket.on('close', function() {
      console.log('Connection Closed');
      devices.splice(devices.indexOf(socket), 1);
  });

  socket.on('error', function(err) {
      console.log('Socket error!', err);
  });

  socket.on('end', function (socket) {
    console.log("SOCKET LEFT");
  });

});

server.listen(4545, function(){
  console.log('server bound');
});


//
// CRON JOBS
//
var fetchActivity = cron.schedule('* * * * *', function() {

  devices.forEach(function(client) {

    //
    // Do not perform a request to the meteor server
    //
    if(client.server_secret){
        return;
    }

    for (i = 0; i < 4; i++) {
      let index = i
      setTimeout(function(){
        var command = JSON.stringify({
          action : 'read',
          arg: index == 0 ? 'activity.json' : `activity${index}.json`
        });

        var message = "Content-Length: "+command.length+"\r\n\r\n"+command+"\r\n"
        console.log('SENDING:', message);

        client.write(message);
      }, 10000 * i);
    }

   });

}, false);
fetchActivity.start();

module.exports = app;
